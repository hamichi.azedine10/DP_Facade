source1 : https://w3sdesign.com
source2 :https://springframework.guru/gang-of-four-design-patterns/facade-pattern/


The intent of the Facade design pattern is to:
"Provide an unified interface to a set of interfaces in a subsystem.
Facade defines a higher-level interface that makes the subsystem
easier to use." [GoF]

The Facade design pattern solves problems like:
How can a simple interface be provided for a complex subsystem?
How can tight coupling between clients and the objects in a subsystem be avoided?
A complex subsystem should provide a simplified (high-level) view that is good enough for most clients that merely need basic functionality.
The Facade pattern describes how to solve such problems:
Provide an unified interface to a set of interfaces in a subsystem:
Facade | operation().
Clients of the subsystem only refer to and know about the (simple) Facade interface and are inde­pen­dent of the many different interfaces in the subsystem, which reduces dependencies and makes clients easier to implement, change, test, and reuse.

The Facade design pattern provides a solution:
Define a separate Facade object that provides an unified interface for a set of interfaces in a subsystem.
 Work through a Facade to minimize dependencies on a subsystem.
 
 The key idea in this pattern is to work through a separate Facade object that provides a simple interface for (already existing) objects in a subsystem.
 Clients can either work with a subsystem directly or its Facade.
 Define a separate Facade object:
 Define an unified interface for a set of interfaces in a subsystem (Facade).
 Implement the Facade interface
 in terms of (by delegating to) the interfaces in the subsystem.
 Working through a Facade object minimizes dependencies on a subsystem (loose coupling), which makes clients easier to implement, change, test, and reuse.
 Clients that need more lower-level functionalities can access the objects in the subsystem directly