public class Facade implements IFacade{
    ClasseA classeA = new ClasseA();
    ClasseB classeB = new ClasseB();
    ClasseC classeC = new ClasseC();
    public  void operation1ABC(){
        System.out.println("operation1ABC");
        classeA.operation1();;
        classeB.operation1();
        classeC.operation1();
    }
    public void  operation2AB(){
        System.out.println("operation2AB");
        classeA.operation2();
        classeB.operation2();
    }
}
