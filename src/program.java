public class program {
    public static void main(String[] args) {
        IFacade facade = new Facade();
        facade.operation1ABC();
        System.out.println("-----------------");
        facade.operation2AB();
    }
}
